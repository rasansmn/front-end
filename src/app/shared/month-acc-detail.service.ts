import { Injectable } from '@angular/core';
import { AccountDetail } from './account-detail.model';
import { HttpClient } from "@angular/common/http";
import { MonthAccDetail } from './month-acc-detail.model';

@Injectable({
  providedIn: 'root'
})
export class MonthAccDetailService {
  formData:AccountDetail;
  readonly rootURL =  '/api';
  list : MonthAccDetail[];
  monthData: MonthAccDetail;

  constructor(private http:HttpClient) { }

  getMonthAccBalance(id){
    return this.http.get(this.rootURL+"/upload/"+id).toPromise();
  }

}
