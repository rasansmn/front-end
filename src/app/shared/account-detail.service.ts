import { Injectable } from '@angular/core';
import { AccountDetail } from './account-detail.model';
import { HttpClient } from "@angular/common/http";
import { MonthAccDetail } from './month-acc-detail.model';

@Injectable({
  providedIn: 'root'
})
export class AccountDetailService {
  formData:AccountDetail;
  readonly rootURL =  '/api';
  list : AccountDetail[];
  monthData: MonthAccDetail[];

  constructor(private http:HttpClient) { }

  postAccountDetail(){
    return this.http.post(this.rootURL+"/AccountDetails", this.formData);
  }

  putAccountDetail(){
    return this.http.put(this.rootURL+"/AccountDetails/"+this.formData.AccId, this.formData);
  }

  deleteAccountDetail(id){
    return this.http.delete(this.rootURL+"/AccountDetails/"+id);
  }

  refreshList(){
    this.http.get(this.rootURL+"/AccountDetails")
    .toPromise()
    .then(res => this.list = res as AccountDetail[]);
  }
}
