import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";

import { AppComponent } from './app.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { AccountDetailComponent } from './account-details/account-detail/account-detail.component';
import { AccountDetailListComponent } from './account-details/account-detail-list/account-detail-list.component';
import { AccountDetailService } from './shared/account-detail.service';
import { MonthAccDetailService } from './shared/month-acc-detail.service';
import { UploadComponent } from './account-details/upload/upload.component';
import { MonthAccDetailComponent } from './account-details/month-acc-detail/month-acc-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountDetailsComponent,
    AccountDetailComponent,
    AccountDetailListComponent,
    UploadComponent,
    MonthAccDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [AccountDetailService, MonthAccDetailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
