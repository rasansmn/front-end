import { Component, OnInit, Input } from '@angular/core';
import { MonthAccDetailService } from 'src/app/shared/month-acc-detail.service';
import { MonthAccDetail } from 'src/app/shared/month-acc-detail.model';

@Component({
  selector: 'app-month-acc-detail',
  templateUrl: './month-acc-detail.component.html',
  styleUrls: ['./month-acc-detail.component.css']
})
export class MonthAccDetailComponent implements OnInit {

  @Input() monthAccData:MonthAccDetail;

  constructor() {
    
  }

  ngOnInit(): void {
  }


}
