import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AccountDetailService } from 'src/app/shared/account-detail.service';
import { AccountDetail } from 'src/app/shared/account-detail.model';
import { ToastrService } from 'ngx-toastr';
import { MonthAccDetailService } from 'src/app/shared/month-acc-detail.service';
import { MonthAccDetail } from 'src/app/shared/month-acc-detail.model';

@Component({
  selector: 'app-account-detail-list',
  templateUrl: './account-detail-list.component.html',
  styles: []
})
export class AccountDetailListComponent implements OnInit {

  @Output() monthAccDetailChange = new EventEmitter<MonthAccDetail>();
  
  constructor(
    public accDetailService: AccountDetailService, 
    public monthlyAccDetailService: MonthAccDetailService, 
    private toastr: ToastrService
    ) { }

  ngOnInit(): void {
    this.accDetailService.refreshList();
  }

  populateForm(pd:AccountDetail){
    this.accDetailService.formData = Object.assign({}, pd);
  }

  populateTable(id){
    this.monthlyAccDetailService.getMonthAccBalance(id)
    .then((res) => 
    {
      this.monthAccDetailChange.emit(res as MonthAccDetail);
    });
    this.accDetailService.refreshList();
  }

  onDelete(id){
    if(confirm('Are you sure to delete this record?')){
      this.accDetailService.deleteAccountDetail(id)
      .subscribe(res => {
        this.accDetailService.refreshList();
        this.toastr.warning('Deleted Successfully', 'Payment Detail Register');
      },
        err=>{
          console.log(err);
        }
      )
    }
  }

  public createFilePath = (serverPath : string) => {
    return 'http://localhost:52039/' + serverPath;
  }

}
