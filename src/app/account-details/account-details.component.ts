import { Component, OnInit } from '@angular/core';
import { MonthAccDetail } from '../shared/month-acc-detail.model';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styles: [
  ]
})
export class AccountDetailsComponent implements OnInit {
  
  monthAccData:MonthAccDetail;

  constructor() { }

  ngOnInit(): void {
  }

  getMonthData(event) {
    this.monthAccData = event as MonthAccDetail;
  }

}
