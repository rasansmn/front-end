import { Component, OnInit } from '@angular/core';
import { AccountDetailService } from 'src/app/shared/account-detail.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-account-detail',
  templateUrl: './account-detail.component.html',
  styles: [
  ]
})
export class AccountDetailComponent implements OnInit {
  public response: { dbPath: ''}

  constructor(public service:AccountDetailService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.resetForm();
  }

  resetForm(form?:NgForm){
    if(form != null)
      form.resetForm();
    this.service.formData = {
      AccId:0,
      Year:'',
      Month:'',
      FilePath:''
    }
  }

  onSubmit(form:NgForm){
    this.service.formData.FilePath = this.response.dbPath;
    if(this.service.formData.AccId == 0)
      this.insertRecord(form);
    else
      this.updateRecord(form);

  }

  insertRecord(form:NgForm){
    this.service.postAccountDetail().subscribe(
      res => {
        this.resetForm(form);
        this.toastr.success('Sumitted Successfully', 'Payment Detail Register');
        this.service.refreshList();
      },
      err => {
        console.log(err);
      }
    )
  }

  updateRecord(form:NgForm){
    this.service.putAccountDetail().subscribe(
      res => {
        this.resetForm(form);
        this.toastr.info('Sumitted Successfully', 'Payment Detail Register');
        this.service.refreshList();
      },
      err => {
        console.log(err);
      }
    )
  }

  public uploadFinished = (event) => {
    this.response = event;
  }
}
